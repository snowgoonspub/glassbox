FROM ubuntu:focal
ARG BASEURL
ENV BASEURL=${BASEURL}
RUN apt-get update
RUN apt-get install -y coreutils git gettext-base wget
RUN DEBIAN_FRONTEND="noninteractive" TZ="Europe/London" apt-get -y install tzdata

# To build our sites we need Hugo
WORKDIR /hugo
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.81.0/hugo_extended_0.81.0_Linux-64bit.deb
RUN dpkg -i hugo_extended_0.81.0_Linux-64bit.deb

# To generate Rustdocs we need Rust/Cargo
RUN apt-get install -y curl clang build-essential
# @todo get rid of the following - it's a hack, we only install this because
# we need it for the Project Maramures docs.  Need to replace with some
# mechanism to automatically discover dependencies (or better still, stop
# Cargo feeling the need to compile everything just to generate docs.)
RUN apt-get install -y libcups2 libcups2-dev
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
RUN chmod +x rustup.sh
RUN ./rustup.sh -y
ENV PATH="/root/.cargo/bin:${PATH}"

# We need npm (urgh) for OpenAPI 3 Generator
RUN apt-get install -y npm
RUN apt-get install -y default-jre
RUN npm install --unsafe-perm=true --allow-root -g @openapitools/openapi-generator-cli && npm cache clean --force

WORKDIR /glassbox
COPY . ./

WORKDIR /
RUN mkdir source
RUN mkdir hugosite
RUN mkdir httpdocs

ENTRYPOINT [ "/glassbox/bin/docker-entrypoint.sh" ]
CMD [ "/source", "/hugosite", "/source/httpdocs" ]


