#!/bin/bash

GBDIR=$(dirname $0)/..

SRCFILE=${1}
SRCDIR=${2}
MDDSTDIR=${3}
HTMLDSTDIR=${4}

SRCFILENAME=`basename "${1}"`
SRCFILEDIR=`dirname "${1}"`
RELSRCPATH=`realpath "--relative-to=${SRCDIR}" "${SRCFILEDIR}"`

# OK...  So, let's generate some Rust docs
(
  cd ${SRCFILEDIR}
  cargo doc --no-deps --document-private-items || exit 2
)
CRATE=`basename "${SRCFILEDIR}"`
mkdir -p "${HTMLDSTDIR}/${RELSRCPATH}"
cp -r "${SRCFILEDIR}"/target/doc/* "${HTMLDSTDIR}/${RELSRCPATH}/."

# And generate a page in Hugo that points the user there
mkdir -p "${MDDSTDIR}/${RELSRCPATH}"
RELHTMLPATH=`realpath --relative-to="${MDDSTDIR}/${RELSRCPATH}" "${HTMLDSTDIR}"`
RELHTMLPATH=${RELHTMLPATH/static\//}

if [ -e "${MDDSTDIR}/${RELSRCPATH}/_index.md" ]; then
cat >> "${MDDSTDIR}/${RELSRCPATH}/_index.md" << EOF

# RustDocs
There are automatically generated RustDocs available for this crate:

* [RustDocs for ${CRATE}](${RELHTMLPATH}/${RELSRCPATH}/${CRATE//-/_}/index.html)

EOF
else
cat > ${MDDSTDIR}/${RELSRCPATH}/_index.md << EOF
+++
title = "${RELSRCPATH}"
+++

# RustDocs
There are automatically generated RustDocs available for this crate:

* [RustDocs for ${CRATE}](${RELHTMLPATH}/${RELSRCPATH}/${CRATE//-/_}/index.html)

EOF
fi
