#!/bin/bash

SRC=${1}
HUGODIR=${2}
OUT=${3}

/glassbox/bin/generate-from-directory.sh "${SRC}" "${HUGODIR}"

#cd ${HUGODIR}
#git config --global user.email "glassbox@snowgoons.com" > /dev/null
#git config --global user.name "GlassBox" > /dev/null
#git init > /dev/null
#git add . > /dev/null
#git commit -m "For Hugo" > /dev/null

#cd ..
hugo -s ${HUGODIR} -d ${OUT}
