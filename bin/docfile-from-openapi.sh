#!/bin/bash

GBDIR=$(dirname $0)/..

SRCFILEDIR=${1}
SRCDIR=${2}
MDDSTDIR=${3}
HTMLDSTDIR=${4}

RELSRCPATH=`realpath "--relative-to=${SRCDIR}" "${SRCFILEDIR}"`
RELHTMLPATH=`realpath --relative-to="${MDDSTDIR}/${RELSRCPATH}" "${HTMLDSTDIR}"`
RELHTMLPATH=${RELHTMLPATH/static\//}

mkdir -p ${MDDSTDIR}/${RELSRCPATH}

if [ -e "${MDDSTDIR}/${RELSRCPATH}/_index.md" ]; then
cat >> "${MDDSTDIR}/${RELSRCPATH}/_index.md" << EOF

# OpenAPI Documentation
There is automatically generated API documentation available:

* [OpenAPI](${RELHTMLPATH}/${RELSRCPATH}/index.html)

EOF
else
cat > ${MDDSTDIR}/${RELSRCPATH}/_index.md << EOF
+++
title = "${RELSRCPATH}"
+++

# OpenAPI Documentation
There is automatically generated API documentation available:

* [OpenAPI](${RELHTMLPATH}/openapi/${RELSRCPATH}/index.html)
EOF
fi

for FILE in "${SRCFILEDIR}"/*.yaml ; do

  echo "Generating OpenAPI docs for ${FILE}"

  mkdir -p "${HTMLDSTDIR}/${RELSRCPATH}"

  npx @openapitools/openapi-generator-cli generate -i ${FILE} -g html2 -o ${HTMLDSTDIR}/${RELSRCPATH}
done

