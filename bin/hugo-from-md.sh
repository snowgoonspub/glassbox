#!/bin/bash

TITLE=${1}
SRCFILE=${2}

cat << EOF
+++
title = "${TITLE}"
+++
EOF

cat ${SRCFILE}