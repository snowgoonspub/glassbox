#!/bin/bash

GBDIR=$(dirname $0)/..

SRCFILE=${1}
SRCDIR=${2}
DSTDIR=${3}

SRCFILENAME=`basename ${1}`
SRCFILEDIR=`dirname ${1}`
RELSRCPATH=`realpath --relative-to=${SRCDIR} ${SRCFILEDIR}`

if [ ${SRCFILENAME} = "README.md" ]; then
  TITLE=${RELSRCPATH}
  OUTFILENAME=_index.md
else
  TITLE=${SRCFILENAME%.*}
  OUTFILENAME=${SRCFILENAME}
fi

if [ ${TITLE} = "." ]; then
  TITLE="Overview"
fi


mkdir -p ${DSTDIR}/${RELSRCPATH}
${GBDIR}/bin/hugo-from-md.sh ${TITLE} ${SRCFILE} > ${DSTDIR}/${RELSRCPATH}/${OUTFILENAME}

